__author__ = "Pierre Galaup - Nicolas Garcia"


class PrivateMessage:
    total = 0
    id = 0

    def __init__(self, subject, text, author_id, recipient_id, read=0, _id=-1):
        if _id == -1:
            self.id = int(PrivateMessage.id)
        else:
            self.id = int(_id)
            if PrivateMessage.id < _id:
                PrivateMessage.id = int(_id)
        self.subject = subject
        self.text = text
        self.author_id = int(author_id)
        self.read = int(read)
        self.recipient_id = []
        self.read = read
        for _id in recipient_id:
            self.recipient_id.append(int(_id))
        PrivateMessage.total += 1
        PrivateMessage.id += 1

    def __del__(self):
        PrivateMessage.total -= 1

    def get_id(self):
        return self.id

    def get_subject(self):
        return self.subject

    def get_text(self):
        return self.text

    def get_author_id(self):
        return self.author_id

    def get_recipient_id(self):
        return self.recipient_id

    def get_read_status(self):
        return self.read

    def jsonify(self):
        json = {
            'id': self.get_id(),
            'subject': self.get_subject(),
            'text': self.get_text(),
            'read': self.get_read_status(),
            'author_id': self.get_author_id(),
            'recipient_id': self.get_recipient_id(),
            'read': self.get_read_status()
        }
        return json