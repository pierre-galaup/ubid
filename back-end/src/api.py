__author__ = "Pierre Galaup - Nicolas Garcia"

from datetime import timedelta
from flask import Flask, jsonify, make_response, request, session
from UbidManager import UbidManager
from Exceptions import ACLError

import utilities

app = Flask(__name__)
app.secret_key = 'A0Zr98j/3yXkokR~XHH!jmN]LWX/,?RT'
app.permanent_session_lifetime = timedelta(minutes=60)

ubid_mng = 0


# Header
@app.after_request
def after_request(data):
    response = make_response(data)
    response.headers['Content-Type'] = 'application/json'
    if 'Origin' in request.headers:
        response.headers['Access-Control-Allow-Origin'] = request.headers['Origin']
        response.headers['Access-Control-Allow-Methods'] = "GET, POST, PUT, DELETE"
        response.headers[
            'Access-Control-Allow-Headers'] = "Origin, X-Requested-With, Content-Type, Accept, Cookie, Set-Cookie"
        response.headers["Access-Control-Allow-Credentials"] = "true"
    return response


# Erreurs
@app.errorhandler(400)
def bad_request(error):
    return make_response(jsonify({'error': 'Bad Request', 'message': "Error in JSON data (or something like that, "
                                                                     "no real details about the error)."}), 400)


@app.errorhandler(401)
def bad_request(error):
    return make_response(jsonify({'error': 'Unauthorized'}), 401)


@app.errorhandler(403)
def bad_request(error):
    return make_response(jsonify({'error': 'Forbidden'}), 403)


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not Found'}), 404)


@app.errorhandler(405)
def not_allowed(error):
    return make_response(jsonify({'error': 'Method Not Allowed'}), 405)


@app.errorhandler(KeyError)
def bad_request_key(error):
    print 'KeyError : ' + error.message.__str__()
    return make_response(
        jsonify({'error': 'Bad Request', 'message': 'key [' + error.message.__str__() + '] is missing in JSON'}), 400)


@app.errorhandler(AssertionError)
def bad_request_assert(error):
    print 'AssertionError : ' + error.message.__str__()
    return make_response(jsonify({'error': 'Bad Request', 'message': error.message.__str__()}), 400)


@app.errorhandler(ValueError)
def bad_request_value(error):
    print 'ValueError : ' + error.message.__str__()
    return make_response(jsonify({'error': 'Bad Request', 'message': error.message.__str__()}), 400)


@app.errorhandler(ACLError)
def bad_request_acl(error):
    print 'ACLError : ' + error.message.__str__()
    return make_response(jsonify({'error': 'Forbidden', 'message': error.message.__str__()}), 403)


# Routes


# Create a new product
@app.route('/products', methods=['POST'])
def add_product():
    utilities.is_authenticated()
    product = ubid_mng.add_product(request.json)
    return jsonify({'product': product}), 201


# List all products
@app.route('/products')
def get_products():
    products = ubid_mng.get_products()
    resp = 200
    if len(products) == 0:
        resp = 204
    return jsonify({'products': products}), resp


# Get the product with the given id
@app.route('/products/<int:product_id>')
def get_product(product_id):
    product = ubid_mng.get_product(product_id)
    return jsonify({'product': product}), 200


# Update a product
@app.route('/products/<int:product_id>', methods=['PUT'])
def update_product(product_id):
    utilities.is_authenticated()
    product = ubid_mng.update_product(product_id, request.json)
    return jsonify({'product_updated': product.jsonify()}), 200


# Search products
@app.route('/search/products', methods=['POST'])
def search_products():
    products = ubid_mng.search(request.json)
    resp = 200
    if len(products) == 0:
        resp = 204
    return jsonify({'search_products': products}), resp

# Search for users
@app.route('/search/users', methods=['POST'])
def search_users():
    users = ubid_mng.user_mng.search(request.json)
    resp = 200
    if len(users) == 0:
        resp = 204
    return jsonify({'search_users': users}), resp

# Search for users ids
@app.route('/search/users/ids', methods=['POST'])
def search_users_ids():
    users_id = ubid_mng.get_ids_by_users(request.json)
    resp = 200
    if len(users_id) == 0:
        resp = 204
    return jsonify({'search_users_ids': users_id}), resp

# Get all PM from user connected
@app.route('/privatemessages')
def get_pms():
    utilities.is_authenticated()
    pms = ubid_mng.get_pms()
    resp = 200
    if len(pms) == 0:
        resp = 204
    return jsonify({'private_messages': pms}), resp


# Get the PM with the given id
@app.route('/privatemessages/<int:pm_id>')
def get_pm(pm_id):
    utilities.is_authenticated()
    pm = ubid_mng.get_pm(pm_id)
    return jsonify({'private_message': pm}), 200


@app.route('/privatemessages/<int:pm_id>/read', methods=['POST'])
def read_pm(pm_id):
    utilities.is_authenticated()
    pm = ubid_mng.read_pm(pm_id)
    return jsonify({'private_message': pm}), 200

# Add a PM
@app.route('/privatemessages', methods=['POST'])
def add_pm():
    utilities.is_authenticated()
    pm = ubid_mng.add_pm(request.json)
    return jsonify({'private_message': pm}), 201


# Register a new user
@app.route('/register', methods=['POST'])
def register():
    ubid_mng.register(request.json)
    return 'Register successful', 201


# Get the user info with the given id
@app.route('/users/<int:user_id>')
def get_user(user_id):
    user = ubid_mng.get_user(user_id)
    return jsonify({'user': user}), 200


# Get the user info with the given id
@app.route('/users/<int:user_id>', methods=['PUT'])
def update_user(user_id):
    user = ubid_mng.update_user(user_id, request.json)
    return jsonify({'user': user}), 200


# Rate the user with the given id
@app.route('/rate/<int:user_id>', methods=['POST'])
def rate_user(user_id):
    utilities.is_authenticated()
    user = ubid_mng.rate_user(user_id, request.json)
    return jsonify({'user': user}), 200


# Get the rating of the user with the given id
@app.route('/rate/<int:user_id>')
def get_rate_user(user_id):
    user = ubid_mng.get_rate_user(user_id)
    return jsonify({'user': user}), 200


# Login
@app.route('/login', methods=['POST'])
def login():
    return_json = ubid_mng.login(request.form['username'], request.form['password'])
    return jsonify({'user_connected': return_json}), 200


# Get the user who is connected
@app.route('/login')
def is_login():
    user = ubid_mng.is_login()
    return jsonify({'user': user}), 200


# Logout
@app.route('/logout', methods=['POST'])
def logout():
    session.pop('username', None)
    return 'Logout successful', 200


# To bid
@app.route('/products/<int:product_id>/bid', methods=['POST'])
def bid(product_id):
    utilities.is_authenticated()
    product = ubid_mng.bid_on_product(product_id, request.json)
    return jsonify({'product': product}), 200


# Add an instant-buy product to cart
@app.route('/products/<int:product_id>/cart', methods=['POST'])
def add_to_cart(product_id):
    utilities.is_authenticated()
    cart = ubid_mng.add_to_cart(product_id)
    return jsonify({'cart': cart}), 200


# Return the cart
@app.route('/cart')
def get_cart():
    utilities.is_authenticated()
    cart = session['cart']
    resp = 200
    if len(cart) == 0:
        resp = 204
    return jsonify({'cart': cart}), resp


# Return all categories available
@app.route('/categories')
def get_categories():
    categories = ubid_mng.get_categories()
    resp = 200
    if len(categories) == 0:
        resp = 204
    return jsonify({'categories': categories}), resp


# Return the category with the given ID
@app.route('/categories/<int:category_id>')
def get_category(category_id):
    category = ubid_mng.get_category(category_id)
    return jsonify({'category:': category}), 200


# Return all products in category and under-categories
@app.route('/categories/<int:category_id>/products')
def get_category_products(category_id):
    products = ubid_mng.get_category_products(category_id)
    resp = 200
    if len(products) == 0:
        resp = 204
    return jsonify({'products': products}), resp


# Return direct categories childs
@app.route('/categories/<int:category_id>/childs')
def get_category_childs(category_id):
    childs = ubid_mng.get_category_childs(category_id)
    return jsonify({'categories': childs})


# Return direct root childs
@app.route('/categories/root/childs')
def get_root_childs():
    childs = ubid_mng.get_category_childs(-1)
    return jsonify({'categories': childs})


# Add a new category
@app.route('/categories', methods=['POST'])
def add_category():
    utilities.is_authenticated()
    category = ubid_mng.add_category(request.json)
    return jsonify({'category': category}), 201


# Get Top Products
@app.route('/categories/top/products')
def get_top_products():
    products = ubid_mng.get_top_products()
    resp = 200
    if len(products) == 0:
        resp = 204
    return jsonify({'products': products}), resp


# Main
if __name__ == '__main__':
    try:
        ubid_mng = UbidManager()
        if not isinstance(ubid_mng, UbidManager):
            raise RuntimeError('System cannot start')
    except Exception as e:
        print 'FATAL : ' + e.message.__str__()
        exit(-1)
    app.run(debug=True)