__author__ = 'Pierre - Nicolas'

from Category import Category


class CategoriesManager:
    total = 0

    def __init__(self):
        self.categories = []
        CategoriesManager.total += 1

    def __del__(self):
        CategoriesManager.total -= 1

    def add_category(self, name, description, image, parent, childs):
        """

        @rtype : Category
        """
        try:
            self.get_category_by_name(name)
        except ValueError:
            if len(name) < 2 or len(description) < 2:
                raise AssertionError('Name or description are too short')
            if parent < -1 or parent > Category.id:
                raise AssertionError('Parent id given is wrong')
            try:
                self.get_category_by_name(name)
                raise AssertionError('Category already exists')
            except ValueError:
                pass
            if parent >= 0:
                try:
                    self.get_category_by_id(parent)
                except ValueError:
                    raise AssertionError('Parent id doesn\'t exists')
            for child_id in childs:
                try:
                    child = self.get_category_by_id(child_id)
                    if child.get_parent() >= 0:
                        raise AssertionError('Child has already a parent')
                except ValueError:
                    raise AssertionError('Child category with id [' + child_id + '] does not exists.')
            category = Category(name, description, image, parent, childs)
            self.categories.append(category)
            for child_id in category.get_childs():
                child = self.get_category_by_id(child_id)
                child.parent = category.get_id()
            return category
        raise ValueError("Category [" + name + "] already exists.")

    def import_category(self, category, to_append=True):
        """

        @rtype : Category
        """
        new_category = Category(category['name'], category['description'], category['image'], category['parent'],
                                category['childs'],
                                category['id'])
        if to_append:
            self.categories.append(new_category)
        else:
            return new_category

    def get_category_by_id(self, _id):
        """

        @rtype : Category
        """
        for category in self.categories:
            if category.get_id() == _id:
                return category
        raise ValueError('Category.id not found')

    def get_category_by_name(self, name):
        """

        @rtype : Category
        """
        for category in self.categories:
            if category.get_name().lower() == name.lower():
                return category
        raise ValueError('Category.name not found')

    def del_category(self, _id):
        for category in self.categories:
            if category.get_id() == _id:
                self.categories.remove(category)
                return True
        raise ValueError('Category.id not found')

    def upt_category(self, _id, name, description, parent): #TODO update for childs
        """

        @rtype : Category
        """
        if len(name) < 2 or len(description) < 2:
            raise AssertionError('Name or description are too short')
        if parent < -1 or parent > Category.id:
            raise AssertionError('Parent id given is wrong')
        try:
            self.get_category_by_name(name)
            raise AssertionError('Category already exists')
        except ValueError:
            pass
        if parent >= 0:
            try:
                self.get_category_by_id(parent)
            except ValueError:
                raise AssertionError('Parent id doesn\'t exists')
        category = self.get_category_by_id(_id)
        category.name = name
        category.description = description
        category.parent = parent
        return category

    def jsonify(self):
        json = []
        for category in self.categories:
            json.append(category.jsonify())
        return json

    def __full_text_jsonify_rec__(self, category_id): # DEPRECATED
        category = self.get_category_by_id(category_id)
        if len(category.get_childs()) == 0:
            return category.jsonify()
        else:
            json = []
            for child_id in category.get_childs():
                try:
                    parent = self.get_category_by_id(category.get_parent()).jsonify()
                except ValueError:
                    parent = -1
                json.append({
                    'id': category.get_id(),
                    'name': category.get_name(),
                    'description': category.get_description(),
                    'childs': self.__full_text_jsonify_rec__(child_id),
                    'parent': parent,
                    'count': category.get_products_count()
                })
            return json

    def full_text_jsonify(self): # DEPRECATED
        json = []
        for category in self.categories:
            childs_json = []
            for child in category.get_childs():
                childs_json.append(self.__full_text_jsonify_rec__(child))
            try:
                parent = self.get_category_by_id(category.get_parent()).jsonify()
            except ValueError:
                parent = -1
            json.append(
                {
                    'id': category.get_id(),
                    'name': category.get_name(),
                    'description': category.get_description(),
                    'childs': childs_json,
                    'parent': parent,
                    'count': category.get_products_count()
                })
        return json

    def get_childs(self, category_id):
        """

        @rtype : List
        """
        childs = []
        if not category_id == -1:
            category = self.get_category_by_id(category_id)
            for child in category.get_childs():
                childs.append(self.get_category_by_id(child).jsonify())
        else:
            for category in self.categories:
                if category.get_parent() == -1:
                    childs.append(category.jsonify())
        return childs

    def is_child_of(self, child_id, parent_id):
        """

        @rtype : Bool
        """
        child = self.get_category_by_id(child_id)
        if child.get_parent() == -1 or child_id == parent_id:
            return False
        elif child.get_parent() == parent_id:
            return True
        elif child.get_parent != parent_id:
            return self.is_child_of(child.get_parent(), parent_id)