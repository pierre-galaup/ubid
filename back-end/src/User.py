__author__ = "Pierre Galaup - Nicolas Garcia"

import hashlib
from CreditCard import CreditCard


class User:
    total = 0
    id = 0

    def __init__(self, username, password, mail, rating, nb_rating, users_rating, _id=-1):
        if _id == -1:
            self.id = int(User.id)
            self.password = hashlib.md5(password).hexdigest()
        else:
            self.password = password
            self.id = int(_id)
            if User.id < _id:
                User.id = int(_id)
        self.username = username
        self.mail = mail
        self.credit_card = 0
        self.rating = rating
        self.nb_rating = nb_rating
        self.users_rating = []
        for user_rating in users_rating:
            self.users_rating.append(int(user_rating))
        User.total += 1
        User.id += 1

    def __del__(self):
        User.total -= 1

    def add_credit_card(self, number, expiration, cryptogram, user_id=-1, _id=-1):
        if user_id == -1:
            self.credit_card = CreditCard(number, expiration, cryptogram, self.id, _id)
        else:
            self.credit_card = CreditCard(number, expiration, cryptogram, user_id, _id)

    def get_username(self):
        return self.username

    def get_password(self):
        return self.password

    def get_mail(self):
        return self.mail

    def get_id(self):
        return self.id

    def get_credit_card(self):
        return self.credit_card

    def get_rating(self):
        return self.rating

    def get_nb_rating(self):
        return self.nb_rating

    def get_users_rating(self):
        return self.users_rating

    def set_username(self, username):
        self.username = username

    def set_password(self, password):
        self.password = hashlib.md5(password).hexdigest()

    def set_mail(self, mail):
        self.mail = mail

    def set_rating(self, rating):
        self.rating = rating

    def set_nb_rating(self, nb_rating):
        self.nb_rating = nb_rating

    def set_users_rating(self, _id):
        self.users_rating.append(int(_id))

    def jsonify_unsafe(self):
        json = {
            'id': self.get_id(),
            'username': self.get_username(),
            'password': self.get_password(),
            'mail': self.get_mail(),
            'rating': self.get_rating(),
            'nb_rating': self.get_nb_rating(),
            'users_rating': self.get_users_rating()
        }
        return json

    def jsonify(self):
        json = {
            'id': self.get_id(),
            'username': self.get_username(),
            'mail': self.get_mail(),
            'rating': self.get_rating(),
            'nb_rating': self.get_nb_rating(),
            'users_rating': self.get_users_rating()
        }
        return json