__author__ = "Pierre Galaup - Nicolas Garcia"

from time import time

from Product import Product
from Exceptions import ACLError
from CategoriesManager import CategoriesManager


class ProductsManager:
    total = 0

    def __init__(self):
        self.products = []
        self.total += 1
        self.categories_mng = CategoriesManager()

    def __del__(self):
        self.total -= 1

    def get_products(self):
        return self.products

    def add_product(self, name, categories, description, image, start_price, vendor_id, start_date, end_date,
                    shipping_cost,
                    is_direct):
        if len(name) < 2 or len(description) < 10:
            raise AssertionError('Too short string')
        if len(name) < 2 or start_price < 0 or shipping_cost < 0 or vendor_id < 0:
            raise AssertionError('Int not correct')
        if int(end_date) <= int(start_date) and is_direct == 0:
            raise AssertionError('Dates are not correct')
        if not isinstance(categories, list) or len(categories) < 1:
            raise AssertionError('Categories are not a list or is empty')
        if not is_direct == 1 and not is_direct == 0:
            raise AssertionError('is_direct must be 0 or 1 only')
        if end_date < start_date + 300 and is_direct == 0:  # 5 minutes est le temps minimal pour une enchere
            raise AssertionError('Wrong period or too short')
        for category in categories:
            try:
                cat = self.categories_mng.get_category_by_id(category)
                cat.increase_products_count()
            except ValueError:
                raise AssertionError('Category with id [' + category.__str__() + '] does not exists')
        product = Product(name, categories, description, image, start_price, start_date, end_date, shipping_cost,
                          vendor_id, is_direct)
        self.products.append(product)
        return product

    def import_product(self, product, to_append=True):
        new_product = Product(product['name'], product['categories'], product['description'], product['image'],
                              product['start_price'],
                              product['start_date'], product['end_date'], product['shipping_cost'],
                              product['vendor_id'], product['is_direct'], product['bidders'], product['bid_count'],
                              product['current_price'],
                              product['id'])
        for category in new_product.get_categories():
            try:
                cat = self.categories_mng.get_category_by_id(category)
                cat.increase_products_count()
            except ValueError:
                raise AssertionError('Category with id [' + category.__str__() + '] does not exists')
        if to_append:
            self.products.append(new_product)
        else:
            return new_product

    def get_product_by_id(self, _id):
        """

        @rtype : Product
        """
        for product in self.products:
            if product.get_id() == _id:
                return product
        raise ValueError('Product.id not found')

    def get_product_by_name(self, name):
        """

        @rtype : Product
        """
        for product in self.products:
            if product.get_name() == name:
                return product
        raise ValueError('Product.name not found')

    def del_product(self, _id):
        for product in self.products:
            if product.get_id() == _id:
                for category in product.get_categories():
                    try:
                        cat = self.categories_mng.get_category_by_id(category)
                        cat.decrease_products_count()
                        if cat.get_products_count() == 0:
                            self.categories_mng.del_category(category)
                    except ValueError:
                        raise AssertionError('Category with id [' + category + '] does not exists')
                self.products.remove(product)
                return True
        raise ValueError('Product.id not found')

    def upt_product(self, _id, name, description, image, categories, start_price, shipping_cost, end_date, is_direct,
                    current_user):
        product = self.get_product_by_id(_id)
        if product.get_vendor_id() != current_user:
            raise ACLError('Not vendor trying to modify bid')
        if len(name) < 2 or len(description) < 10:
            raise AssertionError('Too short string')
        if len(name) < 2 or start_price < 0 or shipping_cost < 0:
            raise AssertionError('Int not correct')
        if (int(end_date) <= int(product.get_start_date())) and is_direct == 0:
            raise AssertionError('Dates are not correct')
        if not isinstance(categories, list) or len(categories) < 1:
            raise AssertionError('Categories are not a list or is empty')
        if (end_date < product.get_start_date() + 300 or end_date <= time()) and is_direct == 0:
            raise AssertionError('Wrong period or too short')
        if len(product.get_bidders()) > 0:
            if start_price != product.get_start_price():
                raise AssertionError('Cannot modify a start price of a bid if there is already bids on it')
            if not is_direct == 0 and not is_direct == 1 and is_direct != product.get_is_direct():
                raise AssertionError('Cannot toggle a bid to an instant-buy if there is already bids on it')
            if product.end_date != end_date:
                raise AssertionError('Cannot change a bid\'s end date if there is already bids on it.')
            if product.get_shipping_cost() != shipping_cost:
                raise AssertionError('Cannot change bid\'s shipping cost if there is already bids on it')

        for category in categories:
            try:
                cat = self.categories_mng.get_category_by_id(category)
                cat.increase_products_count()
            except ValueError:
                raise AssertionError('Category with id [' + category + '] does not exists')
        for category in product.get_categories():
            try:
                cat = self.categories_mng.get_category_by_id(category)
                cat.decrease_products_count()
                if cat.get_products_count() == 0:
                    self.categories_mng.del_category(category)
            except ValueError:
                raise AssertionError('Category with id [' + category + '] does not exists')
        product.name = name
        product.description = description
        product.categories = categories
        product.start_price = start_price
        product.shipping_cost = shipping_cost
        product.end_date = end_date
        product.is_direct = is_direct
        product.image = image
        return product

    def jsonify(self):
        json = []
        for product in self.products:
            json.append(product.jsonify())
        return json

    def get_products_with_category(self, category_id):
        """

        @rtype : List
        """
        try:
            self.categories_mng.get_category_by_id(category_id)
        except ValueError:
            raise AssertionError('Category given does not exists')
        products = []
        for product in self.products:
            for category in product.get_categories():
                if category == category_id:
                    products.append(product.jsonify())
                    break
        return products
