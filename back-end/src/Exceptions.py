__author__ = 'Pierre - Nicolas'

from exceptions import StandardError


class ACLError(StandardError):
    pass
