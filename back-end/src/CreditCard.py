__author__ = "Pierre Galaup - Nicolas Garcia"


class CreditCard:
    total = 0
    id = 0

    def __init__(self, number, expiration, cryptogram, user_id, _id=-1):
        if _id == -1:
            self.id = CreditCard.id
        else:
            self.id = _id
            if CreditCard.id < _id:
                CreditCard.id = _id
        self.user_id = user_id
        self.number = number
        self.expiration = expiration
        self.cryptogram = cryptogram
        CreditCard.total += 1
        CreditCard.id += 1

    def __del__(self):
        CreditCard.total -= 1

    def get_number(self):
        return self.number

    def get_user_id(self):
        return self.user_id

    def get_expiration(self):
        return self.expiration

    def get_cryptogram(self):
        return self.cryptogram

    def get_id(self):
        return self.id

    def jsonify(self):
        json = {
            'id': self.get_id(),
            'number': self.get_number(),
            'expiration': self.get_expiration(),
            'cryptogram': self.get_cryptogram(),
            'user_id': self.get_user_id()
        }
        return json