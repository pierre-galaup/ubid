__author__ = "Pierre Galaup - Nicolas Garcia"

import json

from flask import abort, session


def is_authenticated():
    if 'username' not in session:
        abort(401)


def load_json(path):
    try:
        with open(path, 'r') as json_data:
            try:
                data = json.load(json_data)
            except ValueError:
                return 0
    except IOError:
        return 0
    return data


def save_json(data, path):
    try:
        with open(path, 'w') as outfile:
            json.dump(data, outfile)
    except IOError:
        return -1