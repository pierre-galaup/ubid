__author__ = "Pierre Galaup - Nicolas Garcia"


class Product:
    total = 0
    id = 0

    def __init__(self, name, categories, description, image, start_price, start_date, end_date, shipping_cost,
                 vendor_id,
                 is_direct, bidders={}, bid_count=0, current_price=-1, _id=-1):
        if _id == -1:
            self.id = int(Product.id)
        else:
            self.id = int(_id)
            if Product.id < _id:
                Product.id = int(_id)
        self.name = name
        self.categories = []
        for category in categories:
            self.categories.append(int(category))
        self.description = description
        self.image = image
        self.start_price = float(start_price)
        self.bid_count = int(bid_count)
        self.vendor_id = int(vendor_id)
        self.is_direct = int(is_direct)
        self.bidders = []
        for bidder in bidders:
            self.bidders.append(int(bidder))
        self.start_date = int(start_date)
        self.end_date = int(end_date)
        self.shipping_cost = float(shipping_cost)
        if current_price == -1:
            self.current_price = float(start_price)
        else:
            self.current_price = float(current_price)
        Product.total += 1
        Product.id += 1

    def __del__(self):
        Product.total -= 1

    def get_name(self):
        return self.name

    def get_categories(self):
        return self.categories

    def get_description(self):
        return self.description

    def get_image(self):
        return self.image

    def get_id(self):
        return self.id

    def get_start_price(self):
        return self.start_price

    def get_current_price(self):
        return self.current_price

    def get_shipping_cost(self):
        return self.shipping_cost

    def get_is_direct(self):
        return self.is_direct

    def get_bid_count(self):
        return self.bid_count

    def get_bidders(self):
        return self.bidders

    def get_start_date(self):
        return self.start_date

    def get_end_date(self):
        return self.end_date

    def get_vendor_id(self):
        return self.vendor_id

    def jsonify(self):
        json = {
            'id': self.get_id(),
            'name': self.get_name(),
            'categories': self.get_categories(),
            'description': self.get_description(),
            'image': self.get_image(),
            'start_price': self.get_start_price(),
            'current_price': self.get_current_price(),
            'shipping_cost': self.get_shipping_cost(),
            'is_direct': self.get_is_direct(),
            'bid_count': self.get_bid_count(),
            'bidders': self.get_bidders(),
            'start_date': self.get_start_date(),
            'end_date': self.get_end_date(),
            'vendor_id': self.get_vendor_id()
        }
        return json
