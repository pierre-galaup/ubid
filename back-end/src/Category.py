__author__ = 'Pierre - Nicolas'


class Category:
    total = 0
    id = 0

    def __init__(self, name, description, image, parent, childs, _id=-1):
        if _id == -1:
            self.id = int(Category.id)
        else:
            self.id = int(_id)
            if Category.id < _id:
                Category.id = int(_id)
        self.name = name
        self.description = description
        self.parent = int(parent)
        self.image = image
        self.products_count = 0
        self.childs = []
        for child in childs:
            self.childs.append(int(child))
        Category.total += 1
        Category.id += 1

    def __del__(self):
        Category.total -= 1

    def get_id(self):
        return self.id

    def get_name(self):
        return self.name

    def get_description(self):
        return self.description

    def get_image(self):
        return self.image

    def get_parent(self):
        return self.parent

    def get_childs(self):
        return self.childs

    def increase_products_count(self):
        self.products_count += 1

    def decrease_products_count(self):
        self.products_count -= 1

    def get_products_count(self):
        return self.products_count

    def jsonify(self):
        json = {
            'id': self.get_id(),
            'name': self.get_name(),
            'description': self.get_description(),
            'parent': self.get_parent(),
            'childs': self.get_childs(),
            'count': self.get_products_count(),
            'image': self.get_image()
        }
        return json


