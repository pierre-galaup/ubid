__author__ = 'Pierre - Nicolas'

from time import time

from flask import abort, session

from PrivateMessageManager import PrivateMessageManager
from ProductsManager import ProductsManager
from UsersManager import UsersManager

import utilities


products_path = 'files/products.json'
users_path = 'files/users.json'
credit_cards_path = 'files/creditcards.json'
pm_path = 'files/privatemessages.json'
categories_path = 'files/categories.json'


class UbidManager:
    total = 0

    def __init__(self):
        self.user_mng = UsersManager()
        self.products_mng = ProductsManager()
        self.pm_mng = PrivateMessageManager()
        products = utilities.load_json(products_path)
        users = utilities.load_json(users_path)
        credit_cards = utilities.load_json(credit_cards_path)
        private_messages = utilities.load_json(pm_path)
        categories = utilities.load_json(categories_path)
        if categories != 0:
            for category in categories:
                self.products_mng.categories_mng.import_category(category)
        if products != 0:
            for (product) in products:
                self.products_mng.import_product(product)
        if users != 0:
            for (user) in users:
                self.user_mng.import_user(user)
        if credit_cards != 0:
            for (credit_card) in credit_cards:
                self.user_mng.import_credit_card(credit_card)
        if private_messages != 0:
            for private_message in private_messages:
                self.pm_mng.import_private_message(private_message)
        UbidManager.total += 1

    def __del__(self):
        UbidManager.total -= 1

    def login(self, username, password):
        self.user_mng.login(username, password)
        session['username'] = username
        _user = self.get_user_by_session().jsonify()
        session['cart'] = []
        return _user

    def register(self, json):
        if not json:
            raise ValueError("Request is not JSON")
        self.user_mng.add_user(json['username'], json['password'], json['mail'])
        utilities.save_json(self.user_mng.user_to_json(), users_path)

    def add_product(self, json):
        if not json:
            raise ValueError("Request is not JSON")
        product = self.products_mng.add_product(json['name'], json['categories'], json['description'],
                                                json['image'], json['start_price'], self.get_user_by_session().get_id(),
                                                time(), json['end_date'], json['shipping_cost'], json['is_direct'])
        utilities.save_json(self.products_mng.jsonify(), products_path)
        return product.jsonify()

    # TODO : faire sur les mots au pluriel
    def search(self, json):
        if not json:
            raise ValueError("Request is not JSON")
        try:
            find_items = []
            if int(json['name']) == 1:
                for product in self.products_mng.get_products():
                    is_find = -1
                    for search_term in json['term'].split(" "):
                        search_term += " "
                        if int(json['strict']) == 1:
                            is_find = -1
                        for product_name in product.get_name().split(" "):
                            product_name += " "
                            ret = product_name.lower().find(search_term.lower())
                            if ret > is_find:
                                is_find = ret
                            if is_find == -1 and int(json['description']) == 1:
                                for product_description in product.get_description().split(" "):
                                    product_description += " "
                                    ret = product_description.lower().find(search_term.lower())
                                    if ret > is_find:
                                        is_find = ret
                        if is_find == -1 and int(json['strict']) == 1:
                            break
                        if is_find != -1 and int(json['strict']) == 0:
                            break
                    if is_find != -1:
                        find_items.append(product.jsonify())
            return self.__textify_products__(find_items)
        except KeyError:
            raise KeyError("Error during search")

    def get_products(self):
        return self.products_mng.jsonify()

    def get_product(self, _id):
        products = []
        products.append(self.products_mng.get_product_by_id(_id).jsonify())
        return self.__textify_products__(products)[0]

    def get_user_by_session(self):
        return self.user_mng.get_user_by_username(session['username'])

    def add_pm(self, json):
        if not json:
            raise ValueError("Request is not JSON")
        _id = self.pm_mng.add_private_message(json['subject'], json['text'], self.get_user_by_session().get_id(),
                                              json['recipient_id'])
        utilities.save_json(self.pm_mng.jsonify(), pm_path)
        return self.pm_mng.get_private_message(_id).jsonify()

    def get_pms(self):
        json = []
        pms = self.pm_mng.get_private_messages()
        for pm in pms:
            for id_recipient in pm.get_recipient_id():
                if id_recipient == self.get_user_by_session().get_id():
                    json.append(pm.jsonify())
        return json

    def get_pm(self, pm_id):
        pm = self.pm_mng.get_private_message(pm_id)
        if pm.get_author_id() == self.get_user_by_session().get_id():
            return pm.jsonify()
        abort(401)

    def read_pm(self, pm_id):
        pm = self.pm_mng.get_private_message(pm_id)
        pm.read = 1
        return pm.jsonify()

    def bid_on_product(self, product_id, json):
        bid_price = json['bid_price']
        bid_price = float(bid_price)
        product_to_update = self.products_mng.get_product_by_id(product_id)
        if product_to_update.get_is_direct() == 1:
            raise AssertionError('Cannot bid on a instant-buy product.')
        user = self.get_user_by_session()
        if user.get_credit_card() == 0:
            raise AssertionError('Cannot bid without a Credit Card registred.')
        uid = user.get_id()
        if bid_price < product_to_update.get_current_price() + 0.01:
            raise AssertionError('You cannot bid less or equal than the current price.')
        if time() >= product_to_update.get_end_date():
            raise AssertionError('This sale is over.')
        if product_to_update.get_vendor_id() == uid:
            raise AssertionError("Vendor cannot bid on his own bids.")

        product_to_update.current_price = bid_price
        product_to_update.bid_count += 1
        bidders = product_to_update.get_bidders()
        for bidder in bidders:
            if bidder == uid:
                bidders.remove(bidder)
                break
        bidders.append(uid)
        utilities.save_json(self.products_mng.jsonify(), products_path)
        return product_to_update.jsonify()

    def add_to_cart(self, _id):
        product = self.products_mng.get_product_by_id(_id)
        if product.get_is_direct() == 0:
            raise AssertionError('Cannot add a bid to your cart.')
        if product.get_end_date() <= time():
            raise AssertionError('The sale is over')
        if product.get_vendor_id() == self.get_user_by_session().get_id():
            raise AssertionError('The vendor cannot buy is own products.')
        for item in session['cart']:
            if item == _id:
                raise AssertionError('Already in the cart.')
        session['cart'].append(_id)
        return session['cart']

    def update_product(self, _id, json):
        product = self.products_mng.upt_product(_id, json['name'], json['description'], json['image'],
                                                json['categories'],
                                                json['start_price'], json['shipping_cost'], json['end_date'],
                                                json['is_direct'], self.get_user_by_session().get_id())
        utilities.save_json(self.products_mng.jsonify(), products_path)
        return product

    def get_categories(self):
        return self.products_mng.categories_mng.jsonify()

    def get_category(self, category_id):
        return self.products_mng.categories_mng.get_category_by_id(category_id).jsonify()

    def __textify_products__(self, products):
        for product in products:
            vendor = self.user_mng.get_user_by_id(product['vendor_id'])
            product['vendor'] = {'id': vendor.get_id(), 'username': vendor.get_username(),
                                 'rating': vendor.get_rating()}
            product.pop('vendor_id')
            bidders = []
            for bidder_id in product['bidders']:
                bidder = self.user_mng.get_user_by_id(bidder_id)
                bidders.append({'id': bidder.get_id(), 'username': bidder.get_username()})
            product.pop('bidders')
            product['bidders'] = bidders
        return products

    def get_category_products(self, category_id):
        products = self.products_mng.get_products_with_category(category_id)
        return self.__textify_products__(products)

    def add_category(self, json):
        if not json:
            raise ValueError('Request is not JSON')
        category = self.products_mng.categories_mng.add_category(json['name'], json['description'], json['image'],
                                                                 json['parent'],
                                                                 json['childs'])
        utilities.save_json(self.products_mng.categories_mng.jsonify(), categories_path)
        return category.jsonify()

    def get_user(self, user_id):
        user = self.user_mng.get_user_by_id(user_id).jsonify()
        return user

    def get_full_text_categories(self):
        return self.products_mng.categories_mng.full_text_jsonify()

    def get_category_childs(self, category_id):
        return self.products_mng.categories_mng.get_childs(category_id)

    def is_login(self):
        if 'username' not in session:
            abort(405)
        return self.get_user_by_session().jsonify()

    def get_top_products(self):
        products = []
        for product in self.products_mng.products:
            if (product.get_bid_count() > 10 and product.get_end_date() > time()) or \
                    (product.get_end_date() > time() and product.get_end_date() - time() < 3600):
                products.append(product.jsonify())
        return self.__textify_products__(products)

    def rate_user(self, user_id, json):
        for user in self.user_mng.get_users():
            if user.get_id() == user_id:
                json = self.user_mng.rate_user(user, json['rating'], self.get_user_by_session().get_id())
        utilities.save_json(self.user_mng.user_to_json(), users_path)
        return json

    def get_rate_user(self, user_id):
        user = self.user_mng.get_user_by_id(user_id)
        _user = {
            'rating': user.get_rating(),
            'nb_rating': user.get_nb_rating()
        }
        return _user

    def get_ids_by_users(self, json):
        if not json:
            raise ValueError('Request is not JSON')
        required_users = json['users']
        users = []
        for username in required_users:
            try:
                user = self.user_mng.get_user_by_username(username)
            except ValueError:
                users.append({'username': username, 'id': -1})
            else:
                users.append({'username': username, 'id': user.get_id()})
        return users

    def update_user(self, user_id, json):
        if not json:
            raise ValueError('Request is not JSON')
        self.user_mng.upt_user(user_id, json['username'], json['password'], json['mail'], json['credit_card'])