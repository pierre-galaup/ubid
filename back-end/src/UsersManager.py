import hashlib

__author__ = "Pierre Galaup - Nicolas Garcia"

from flask import abort
from User import User


class UsersManager:
    total = 0

    def __init__(self):
        self.users = []
        UsersManager.total += 1

    def __del__(self):
        UsersManager.total -= 1

    def add_user(self, username, password, mail):
        try:
            self.get_user_by_username(username)
        except ValueError:
            new_user = User(username, password, mail, 0, 0, [])
            self.users.append(new_user)
            return
        raise ValueError("User [" + username + "] already registred")

    def import_user(self, user):
        new_user = User(user['username'], user['password'], user['mail'], user['rating'], user['nb_rating'],
                        user['users_rating'], user['id'])
        self.users.append(new_user)

    def import_credit_card(self, credit_card):
        user_id = credit_card['user_id']
        for user in self.users:
            if user.get_id() == user_id:
                user.add_credit_card(credit_card['number'], credit_card['expiration'],
                                     credit_card['cryptogram'], user.get_id(), credit_card['id'])
                break

    def upt_user(self, _id, username, password, mail, credit_card):
        to_update = self.get_user_by_id(_id)
        if to_update.get_username() != username and username != "":
            try:
                self.get_user_by_username(username)
            except ValueError:
                raise ValueError("User [" + username + "] already exists")
            to_update.set_username(username)
        if to_update.get_mail() != mail and mail != "":
            to_update.set_mail(mail)
        if password != "":
            to_update.set_password(password)
        if credit_card is not None:
            if to_update.get_credit_card() == 0:
                to_update.add_credit_card(credit_card['number'], credit_card['expiration'], credit_card['cryptogram'])
            else:
                to_update.credit_card.number = credit_card['number']
                to_update.credit_card.cryptogram = credit_card['cryptogram']
                to_update.credit_card.expiration = credit_card['expiration']
        return to_update.jsonify()


    def get_users(self):
        return self.users

    def get_user_by_id(self, _id):
        """

        @rtype : User
        """
        for user in self.users:
            if user.get_id() == _id:
                return user
        raise ValueError('ID not found')

    def get_user_by_username(self, username):
        """

        @rtype : User
        """
        for user in self.users:
            if user.get_username().lower() == username.lower():
                return user
        raise ValueError('Username not found')

    def del_user(self, _id):
        for user in self.users:
            if user.get_id() == _id:
                self.users.remove(user)
                break

    def login(self, username, password):
        to_login = self.get_user_by_username(username)
        if to_login.get_password() != hashlib.md5(password).hexdigest():
            abort(403)

    @staticmethod
    def rate_user(user, rating, id_who_rate):
        if rating < 0 or rating > 100:
            raise ValueError('The rating need to be between 0 and 100.')
        for user_who_rate in user.get_users_rating():
            if user_who_rate == id_who_rate:
                raise AssertionError('User already rate this user.')
        prev_rating = user.get_rating()
        nb_rating = user.get_nb_rating()
        new_rating = ((prev_rating * nb_rating) + rating) / (nb_rating + 1)
        nb_rating += 1
        user.set_nb_rating(nb_rating)
        user.set_rating(new_rating)
        user.set_users_rating(id_who_rate)
        _user = {
            'rating': new_rating,
            'nb_rating': nb_rating
        }
        return _user

    def user_to_json(self):
        json = []
        for user in self.users:
            json.append(user.jsonify_unsafe())
        return json

    def credit_cards_to_json(self):
        json = []
        for user in self.users:
            if user.get_credit_card() != 0:
                json.append(user.get_credit_card().jsonify)
        return json

    def search(self, json):
        if not json:
            raise ValueError("Request is not JSON")
        to_find = json['name']
        to_find = to_find.lower()
        find_users = []
        len_to_find = len(to_find)
        for user in self.users:
            username = user.get_username().lower()
            len_username = len(username)
            if len_to_find <= len_username:
                index = 0
                match = True
                while index < len_to_find and match:
                    if username[index] == to_find[index]:
                        match = True
                    else:
                        match = False
                    index += 1
                if match:
                    find_users.append(user.jsonify())
        return find_users