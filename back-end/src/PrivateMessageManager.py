__author__ = "Pierre Galaup - Nicolas Garcia"

from PrivateMessage import PrivateMessage


class PrivateMessageManager:
    total = 0

    def __init__(self):
        self.private_messages = []
        PrivateMessageManager.total += 1

    def __del__(self):
        PrivateMessageManager.total -= 1

    def add_private_message(self, subject, text, author_id, recipient_id):
        new_pm = PrivateMessage(subject, text, author_id, recipient_id, 0)
        self.private_messages.append(new_pm)
        return new_pm.get_id()

    def import_private_message(self, private_message):
        new_private_message = PrivateMessage(private_message['subject'], private_message['text'],
                                             private_message['author_id'], private_message['recipient_id'],
                                             private_message['read'], private_message['id'])
        self.private_messages.append(new_private_message)

    def get_private_messages(self):
        return self.private_messages

    def get_private_message(self, _id):
        """

        @rtype : PrivateMessage
        """
        for private_message in self.private_messages:
            if private_message.get_id() == _id:
                return private_message
        raise ValueError('ID not found')

    def del_private_message(self, _id):
        for private_message in self.private_messages:
            if private_message.get_id() == _id:
                self.private_messages.remove(private_message)
                break

    def jsonify(self):
        json = []
        for private_message in self.private_messages:
            json.append(private_message.jsonify())
        return json
