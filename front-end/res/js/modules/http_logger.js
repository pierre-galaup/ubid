
// perform logging of requests
angular.module('http-logger-module', [])
// where are logged all the requests
.value('http_history',
{
  history: [],
  alerts: {},
  print_success: false,
})
// perform the logging
// private
.factory('_http-logger-on-end', function ($q, http_history, $timeout)
{
  var alert_ids = 0;
  return function(promise)
  {
    return promise.then(function(resp)
    {
      if (resp.config === undefined)
        return resp;

      http_history.history.unshift({time: moment().format('HH:mm:ss'), error: false, data: resp, show_header: false, show_content: false});
      var is_error = angular.isObject(resp.data) && resp.data.__status != 'success';
      if (is_error || http_history.print_success)
      {
        var id = alert_ids++;
        http_history.alerts[id] =
        {
          type: is_error ? "error" : "success",
          title: angular.isString(resp.data.title) ? resp.data.__status + ": " + resp.data.title : is_error ? "error" : "success",
          content: angular.isString(resp.data.reason) ? resp.data.reason : resp.config.url,
        };
        $timeout(function()
        {
          delete http_history.alerts[id];
        }, 3500);
      }
      return resp;
    }, function(resp)
    {
      if (resp.config === undefined)
        return $q.reject(resp);

      http_history.history.unshift({time: moment().format('HH:mm:ss'), error: true, data: resp});
      var id = alert_ids++;
      http_history.alerts[id] =
      {
        type: "error",
        title: "error " + resp.__status,
        content: resp.config.url,
      };
      $timeout(function()
      {
        delete http_history.alerts[id];
      }, 7000);
      return $q.reject(resp);
    });
  };
})
.config(function($httpProvider)
{
  $httpProvider.responseInterceptors.push('_http-logger-on-end');
})
.controller('http_logs', function($scope, http_history)
{
  $scope.http_history = http_history;
})
