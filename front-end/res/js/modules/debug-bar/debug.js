
angular.module('debug-bar', ['http-logger-module'])
.controller('debug', function ($scope, $http, http_history, $rootScope)
{
  $scope.http_history = http_history;

  $scope.debug_http_visible = false;

  $scope.base_url = '/server.php?';
  $scope.data = '';

  $scope.submit_url = function()
  {
    $http.post($scope.base_url +  $scope.data).success(function(response)
    {
    });
  }
  $scope.need_auth = function()
  {
    $rootScope.$broadcast('event:auth-loginRequired');
  }
})
