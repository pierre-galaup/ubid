

angular.module('ubid')
.service('page_stack', function($compile, $location)
{
  this.pseudo_url = "/";

  this.page_stack = [];
  this.current_page =
  {
    element: undefined,
    name: 'home',
  };

  this.compute_url = function()
  {
    var tpseudo_url = '';

    _.each(this.page_stack, function(e)
    {
      tpseudo_url += '-' + e.name;
    });

    this.pseudo_url = tpseudo_url + "-" + this.current_page.name;
    $location.hash(this.pseudo_url);
  };
  this.compute_url();

  this.pop_page = function(idx)
  {
    if (this.current_page.element === undefined)
      this.current_page.element = $('.page');

    // remove current page and replace it with the page at index idx in the page stack
    idx = parseInt(idx) || 0;

    if (idx != 0)
    {
      // remove pages
      while (idx)
      {
        this.page_stack[this.page_stack.length - 1].element.remove();
        this.page_stack.pop();
        idx--;
      }
    }
    // remove current_page
    this.current_page.element.remove();
    // replace it with the last
    this.current_page.name = this.page_stack[this.page_stack.length - 1].name;
    this.current_page.element = this.page_stack[this.page_stack.length - 1].element;

    this.page_stack.pop();

//     if (this.page_stack.length >= 1)
//       this.page_stack[this.page_stack.length - 1].element.removeClass('invisible-page');

    this.current_page.element.unbind('mousedown', this.pop_page);

    // make it visible
    this.current_page.element.removeClass('reduced-page invisible-page');

    this.compute_url();
  };
  this.clear_page_stack = function()
  {
    _.each(this.page_stack, function(e)
    {
      e.element.remove();
    });
    this.page_stack = [];
    this.compute_url();
  }
  this.push_current_page = function($scope, name, template, controller)
  {
    if (this.current_page.element === undefined)
      this.current_page.element = $('.page');

    if (name != this.current_page.name)
    {
      this.current_page.element.addClass('reduced-page invisible-page');
      this.current_page.element.on('mousedown', this.pop_page);

      this.page_stack.push(_.clone(this.current_page));
    }
    else
      this.current_page.element.remove();

//     if (this.page_stack.length > 1)
//       this.page_stack[this.page_stack.length - 1].element.addClass('invisible-page');

    if (controller)
      controller = 'data-ng-controller="' + controller + '"';
    else
      controller = '';

    // create a new page
    this.current_page.name = name;
    $('.page-container > .clearfix').before('<div class="page"><span data-ng-include="\''+template+'\'" ' + controller + '></span></div>');
    this.current_page.element = $('.page-container > .page:last');

    this.compute_url();

    $compile(this.current_page.element)($scope);
  };
})

// the controller that uses the service
.controller('page-ctrl', function($scope, page_stack, user)
{
  // bindings
  $scope.push_current_page = _.partial(_.bind(page_stack.push_current_page, page_stack), $scope);
  $scope.pop_page = _.bind(page_stack.pop_page, page_stack);
  $scope.clear_page_stack = _.bind(page_stack.clear_page_stack, page_stack);

  $scope.page_stack = page_stack;
  $scope.user = user;
})

