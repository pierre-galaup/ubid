angular.module('ubid')
.value('user',
{
    is_logged: false,
    rating: 50,
    picture: 'http://lorempixel.com/450/450/people',//'res/images/default-user.png', //FIXME Unsupported by api
    username: '',
    mail: '',
    id: 0,
    fav_notifs: 2,
    unread_messages: 0,
    cart: [],

    messages: [],
})
.run(function($http, user_mgr)
{
  // query if the user is already logged in
  user_mgr.refresh();
})
// everything that manage interaction with the API for users
.service('user_mgr', function($http, server, user, $timeout)
{
  var reset_user = function()
  {
    user.is_logged = false;
    user.rating = 0;
    user.username = '';
    user.mail = '';
    user.id = 0;
    user.fav_notifs = 0;
    user.unread_messages = 0;
    user.messages.length = 0;
    user.cart.length = 0;
    //this.clear_cart();
  }
  var umgr = this;

  var start_polling = function()
  {
    var polling_time = 5500;

    var poller = function()
    {
      umgr.poll_messages();

      if (user.is_logged)
       $timeout(poller, polling_time);
    };
    $timeout(poller, polling_time);
  }

  this.logout = function()
  {
    $http.post(server.get_url('/logout'));
    reset_user();
  };

  // callback is function(bool success, string message)
  this.login = function(username, password, callback)
  {
    var cred = $.param({ username: username, password: password });
    var umgr = this;

    $http(
    {
      method: 'POST',
      url: server.get_url('/login'),
      data: cred,
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    })
    .success(function(data, status, headers, config)
    {
      _.each(data.user_connected, function(v, k)
      {
        user[k] = v;
      });
      user.is_logged = true;
      umgr.poll_messages();
      umgr.refresh_cart(function(){});

      start_polling();

      callback(true, "");
    })
    .error(function(data, status, headers, config)
    {
      user.is_logged = false;
      user.username = '';
      user.id = 0;

      var status = '';
      if (data.message !== undefined)
        status = data.message;
      else if (status == 403)
        status = "Bad password";
      else
        status = "Failed to sign in";

      callback(false, status);
    });
  };

  // query again the server for the user
  this.refresh = function()
  {
    var umgr = this;
    $http.get(server.get_url('/login'))
    .success(function(data, status, headers, config)
    {
      _.each(data.user, function(v, k)
      {
        user[k] = v;
      });
      user.is_logged = true;
      umgr.poll_messages();
      umgr.refresh_cart();

      start_polling();

    })
    .error(function(data, status, headers, config)
    {
      user.is_logged = false;
      user.username = '';
      user.id = 0;
    });
  };

  this.register = function(username, password, mail, success_cb, error_cb)
  {
    success_cb = success_cb || function(){};
    error_cb = error_cb || function(){};

    if (!user.is_logged) // huh... it's a bit dumb ;)
    {
      var cred = JSON.stringify({ username: username, password: password, mail: mail });
      $http(
      {
        method: 'POST',
        url: server.get_url('/register'),
        data: cred,
        headers: {'Content-Type': 'application/json'}
      })
      .success(function(data, status, headers, config)
      {
        success_cb(data, status, headers, config);
      })
      .error(function(data, status, headers, config)
      {
        error_cb(data, status, headers, config);
      })
    }
  };


  this.search_user = function(username, callback)
  {
    $http(
    {
      method: 'GET',
      url: server.get_url('/search/users'),
      data: $.param({name: username}),
    })
    .success(function(data, status, headers, config)
    {
      callback(true, data.search_users);
    })
    .error(function(data, status, headers, config)
    {
      callback(false, data);
    });
  };


  // HACK.
  this.get_user_info_from_username = function(username, callback)
  {
    $http(
    {
      method: 'GET',
      url: server.get_url('/search/users'),
      data: $.param({name: username}),
    })
    .success(function(data, status, headers, config)
    {
      if (data.search_users.length == 1 && data.search_users[0].username == username)
        callback(true, data.search_users[0]);
      else
        callback(false, null); // bad username
    })
    .error(function(data, status, headers, config)
    {
      callback(false, data);
    });
  };

  this.get_user_info_from_id = function(userid, callback)
  {
    $http(
    {
      method: 'GET',
      url: server.get_url('/users/' + userid),
    })
    .success(function(data, status, headers, config)
    {
      callback(true, data);
    })
    .error(function(data, status, headers, config)
    {
      callback(false, data);
    });
  };

  // return an object of name/ids from an array of usernames.
  // calback(success, obj)
  this.get_user_ids = function(users, callback)
  {
    $http(
    {
      method: 'POST',
      url: server.get_url('/search/users/ids'),
      data: JSON.stringify({users: users}),
    })
    .success(function(data, status, headers, config)
    {
      callback(true, data);
    })
    .error(function(data, status, headers, config)
    {
      callback(false, data);
    });
  };

  this.rate_user = function(user_id, score, callback)
  {
    $http(
    {
      method: 'POST',
      url: server.get_url('/rate/' + user_id),
      data: JSON.stringify({rating: score}),
    })
    .success(function(data, status, headers, config)
    {
      callback(true, "");
    })
    .error(function(data, status, headers, config)
    {
      callback(false, data.message);
    });
  };

  // retrieve all pms
  this.poll_messages = function(on_error)
  {
    on_error = on_error || function(){};

    if (user.is_logged) // huh... it's a bit dumb ;)
    {
      $http(
      {
        method: 'GET',
        url: server.get_url('/privatemessages'),
      })
      .success(function(data, status, headers, config)
      {
        // update unread messages count:
        user.unread_messages = 0;
        var idx = 0;

        // we assume here no messages can be deleted
        // user.messages.length = 0;
        _.each(data.private_messages, function(o)
        {
          if (!o.read)
            ++user.unread_messages;
          user.messages[idx] = user.messages[idx] || {};
          _.each(o, function(v, k)
          {
            user.messages[idx][k] = v;
          });
          ++idx;
        });


      })
      .error(function(data, status, headers, config)
      {
        on_error(data);
      })
    }
  };

  this.mark_as_read = function(message_id)
  {
    var do_rq = false;
    _.each(user.messages, function(m)
    {
      if (m.id == message_id && m.read == false)
      {
        do_rq = true;
        if (!m.marked_as_read)
        {
          --user.unread_messages;
          m.marked_as_read = true;
        }
      }
    });

    if (do_rq)
      $http({method: 'POST', url: server.get_url('/privatemessages/' + message_id + '/read')});
  };

  this.send_message = function(title, message, recipients, callback)
  {
    $http(
      {
        method: 'POST',
        url: server.get_url('/privatemessages'),
        data: JSON.stringify({subject:title, text:message, recipient_id: recipients}),
        headers: {'Content-Type': 'application/json'}
      })
      .success(function(data, status, headers, config)
      {
        callback(true, data);
      })
      .error(function(data, status, headers, config)
      {
        callback(false, data);
      });
  };

  // recipients is an array of username
  // callback is: callback(success, data)
  this.send_message_to_users = function(title, message, recipients, callback)
  {
    this.get_user_ids(recipients, function(success, mu_ids)
    {
      if (!success)
        return callback(false, ids);

      var err_msg = '';
      var ids = [];
      _.each(mu_ids.search_users_ids, function(v)
      {
        if (v.id < 0)
          err_msg = (err_msg.length ? ',' : '') + ' ' + v.username;
        ids.push(v.id);
      });

      // could not find all users
      if (err_msg.length)
        return callback(false, {message: 'could not find those users: ' + err_msg});

      $http(
      {
        method: 'POST',
        url: server.get_url('/privatemessages'),
        data: JSON.stringify({subject: title, text: message, recipient_id: ids}),
        headers: {'Content-Type': 'application/json'}
      })
      .success(function(data, status, headers, config)
      {
        callback(true, data);
      })
      .error(function(data, status, headers, config)
      {
        callback(false, data);
      })
    });
  };

  this.save = function(user_updated, password, creditcard, success_cb, error_cb)
  {
    success_cb = success_cb || function(){};
    error_cb = error_cb || function(){};

    var tmp = { username: user_updated.username, mail: user_updated.mail, credit_card: creditcard, password: password || "" };

    $http(
    {
      method: 'PUT',
      url: server.get_url('/users/' + user.id),
      data: JSON.stringify(tmp),
      headers: {'Content-Type': 'application/json'}
    })
    .success(function(data, status, headers, config)
    {
      umgr.refresh();
      success_cb(data, status, headers, config);
    })
    .error(function(data, status, headers, config)
    {
      error_cb(data, status, headers, config);
    })
  };

  // refresh the cart
  this.refresh_cart = function(callback) {
    callback = callback || function(){};
    $http(
    {
      method: 'GET',
      url: server.get_url('/cart')
    })
    .success(function(data, status, headers, config)
    {
      if (status == 204)
        user.cart.length = 0;
      else
        angular.extend(user.cart, data.cart);
      callback(true, data);
    })
    .error(function(data, status, headers, config)
    {
      callback(false, data);
    })
  };

  this.clear_cart = function() {
    //FIXME Unsupported by api
  };

  this.add_to_cart = function(product, callback) {
      callback = callback || function(){};
      $http(
      {
        method: 'POST',
        url: server.get_url('/products/' + product.id + '/cart')
      })
      .success(function(data, status, headers, config)
      {
        user.cart = data.cart;
        callback(true, data);
      })
      .error(function(data, status, headers, config)
      {
        callback(false, data);
      })
  };

  this.is_in_cart = function(product) {
    for (var i = 0; i < user.cart.length; i++) {
      if (user.cart[i] == product.id) {
        return true;
      }
    }
    return false;
  };
});
