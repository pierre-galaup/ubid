angular.module('ubid')
.value('category',
{
  active_product: null,
  active_category: null,
  category_list: [],
})

// the root category
.constant('root_category', 'root')
.constant('top_products', 'top')
.constant('fav_products', 'fav')

// fetch the main category
.run(function(category_mgr, top_products)
{
//   category_mgr.get_category(top_products);
})

// actions that could be done on categories
.service('category_mgr', function($http, server, user, category, root_category)
{
  // ### listing

  // callback is function(bool success, string message)
  // the result is placed in category.active_category
  // it will return both the products and the sub-categories that belongs to this category.
  this.get_category = function(id, callback)
  {
    callback = callback || function(){};

    $http.get(server.get_url('/categories/' + id + '/childs'))
    .success(function(data, status)
    {
      category.active_category = {products: []};

      if (status == 200)
        category.active_category = data;

      // as categories are no dls, get the products
      if (id != root_category)
      {
        $http.get(server.get_url('/categories/' + id + '/products'))
        .success(function(data, status)
        {
          if (status == 200)
            category.active_category.products = data.products;
          callback(true, "");
        })
        .error(function(data, status)
        {
          callback(true, "error"); // YAY.
        });
      }
      else
        callback(true, "");
    })
    .error(function(data, status)
    {
      console.log(data);
      callback(false, "");
    });
  };

  // callback is function(bool success, string message)
  // the result is placed in category.active_category
  // it will return only the products that belongs to this category.
  this.get_category_products = function(id, callback)
  {
    callback = callback || function(){};

    $http.get(server.get_url('/categories/' + id + '/products'))
    .success(function(data, status)
    {
      category.active_category = {products: []};
      if (status == 200)
        category.active_category.products = data.products;
      callback(true, "");
    })
    .error(function(data, status)
    {
      callback(false, ""); // YAY.
    });
  };

  // callback is function(bool success, string message)
  // the result is placed in category.active_product
  this.get_product = function(id, callback)
  {
    callback = callback || function(){};

    $http.get(server.get_url('/products/' + id))
    .success(function(data, status)
    {
      category.active_product = data.product;

      callback(true, ""); // YAY.
    })
    .error(function(data, status)
    {
      console.log(data);
      callback(false, "");
    });
  };

  // refresh the current product
  this.refresh_product = function()
  {
    $http.get(server.get_url('/products/' + category.active_product.id))
    .success(function(data, status)
    {
      _.each(data.product, function(v, k)
      {
        category.active_product[k] = v;
      });
    });
  };

  // callback is function(bool success, string message)
  // results are placed in category.active_category
  this.search_products = function(string, strict, callback)
  {
    callback = callback || function(){};

    var request = JSON.stringify({term: string, name: 1, description: 1, strict: strict});
    $http(
    {
      method: "POST",
      url: server.get_url('/search/products'),
      data: request,
      headers: {'Content-Type': 'application/json'}
    })
    .success(function(data, status)
    {
      category.active_category = {products: []};

      if (status == 200)
        category.active_category.products = data.search_products;

      callback(true, ""); // YAY.
    })
    .error(function(data, status)
    {
      callback(false, "");
    });
  };

  // make a bid on a product
  // callback is function(bool success, string message)
  this.bid_on_product = function(product_id, bid_price, callback)
  {
    var cat_mgr = this;
    $http(
    {
      method: "POST",
      url: server.get_url('/products/' + product_id + '/bid'),
      data: JSON.stringify({bid_price: bid_price}),
      headers: {'Content-Type': 'application/json'}
    })
    .success(function(data, status)
    {
      if (product_id == category.active_product.id)
        cat_mgr.refresh_product();
      callback(true, "");
    })
    .error(function(data, status)
    {
      cat_mgr.refresh_product();
      callback(false, data.message);
    });
  };

  // bid on the active product
  // callback is function(bool success, string message)
  this.bid_on_active_product = function(bid_price, callback)
  {
    this.bid_on_product(category.active_product.id, bid_price, callback);
  };

  // retrieve full cat list
  this.refresh_cat_list = function()
  {
    var cat_mgr = this;
    $http(
    {
      method: "GET",
      url: server.get_url('/categories'),
    })
    .success(function(data, status)
    {
      category.category_list.length = 0;
      angular.extend(category.category_list, data.categories);
    })
  };
  this.refresh_cat_list();

  // ### editing/creating
  this.add_product = function(product, callback)
  {
    var cat_mgr = this;
    $http(
    {
      method: "POST",
      url: server.get_url('/products'),
      data: JSON.stringify(product),
      headers: {'Content-Type': 'application/json'}
    })
    .success(function(data, status)
    {
      category.active_product = data.product;
      callback(true, data.product);
    })
    .error(function(data, status)
    {
      callback(false, data.message);
    });
  };

  this.update_product = function(product, callback)
  {
    var cat_mgr = this;
    $http(
    {
      method: "PUT",
      url: server.get_url('/products/' + product.id),
      data: JSON.stringify(product),
      headers: {'Content-Type': 'application/json'}
    })
    .success(function(data, status)
    {
      category.active_product = data.product;
      callback(true, data.product);
    })
    .error(function(data, status)
    {
      callback(false, data.message);
    });
  };

  this.delete_product = function(product_id, callback)
  {
    var cat_mgr = this;
    $http(
    {
      method: "DELETE",
      url: server.get_url('/products/' + product_id),
    })
    .success(function(data, status)
    {
      if (category.active_product.id == product_id)
        category.active_product = {};
      callback(true, data.product);
    })
    .error(function(data, status)
    {
      callback(false, data.message);
    });
  };
})

