
// five star rating, based on a [0;100] score

angular.module('ubid')
.directive('ubidSlider', function($interval)
{
  return {
    restrict: 'A',
    scope:
    {
      delay: '=',
    },
    link: function(scope, elem, attrs)
    {
      elem.addClass('ubid-slider');

      var arrow_next = elem.children('.slider-next');
      var arrow_prev = elem.children('.slider-prev');
      var container = elem.children('.slider-content');
      var counter = 0;

      // used in mouse handlers
      var stop_auto_scroll = false;

      // some mouse handlers
      elem.hover
      (
        function() // on enter
        {
          stop_auto_scroll = true;
        },
        function() // on leave
        {
          stop_auto_scroll = false;
        }
      );

      // prev and next handlers
      arrow_next.click(function()
      {
        var num_slides = container.children('.slider-element').size();

        counter = (counter + 1) % num_slides;

        container.css('top', (counter * -100) + '%');
      });
      arrow_prev.click(function()
      {
        var num_slides = container.children('.slider-element').size();

        if (--counter < 0)
          counter = num_slides - 1;

        container.css('top', (counter * -100) + '%');
      });

      // the autoscroll function
      $interval(function()
      {
        if (stop_auto_scroll)
          return;

        var num_slides = container.children('.slider-element').size();

        counter = (counter + 1) % num_slides;

        container.css('top', (counter * -100) + '%');
      }, scope.delay * 1000);
    }
  };
});
