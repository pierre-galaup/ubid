
// create
angular.module('ubid')
.controller('add-product-ctrl', function($scope, category_mgr, category)
{
  $scope.product =
  {
    name: '',
    description: '',
    category: -1,
    is_direct: false,
    start_price: 0.01,
    shipping_cost: 0,
    image: 'http://lorempixel.com/750/450/technics',
    end_date: Date.now() + 1000000,
  };
  $scope.categories = category.category_list;
  $scope.status = {agreement: false};
  $scope.status_messages = [];

  $scope.create_product = function()
  {
    $scope.status_messages.length = 0;

    if ($scope.add_form.$invalid)
      $scope.status_messages.push('Some of the fields arn\'t valid/filled.');

    if (!$scope.status.agreement)
      $scope.status_messages.push('You have to agree with the legal note !!!');

    if ($scope.product.category == -1)
      $scope.status_messages.push('You have to chose a category for the product !!!');

    if ($scope.status_messages.length)
      return;

    var product = _.clone($scope.product);
    product.end_date /= 1000;
    product.categories = [product.category];
    category_mgr.add_product(product, function(success, data)
    {
      if (success) // redirect to product
      {
        category_mgr.refresh_product();
        $scope.push_current_page(category.active_product.name, 'templates/product-view.html', 'product-view-ctrl');
        $scope.clear_page_stack();
      }
      else
        $scope.status_messages.push(data);
    });

  };
})

// update
.controller('upd-product-ctrl', function($scope, category_mgr, category)
{
  $scope.status_messages = [];

  $scope.update_product = function()
  {
    $scope.status_messages.length = 0;

    if ($scope.upd_form.$invalid)
      $scope.status_messages.push('Some of the fields arn\'t valid/filled.');

    if ($scope.product_cpy.category == -1)
      $scope.status_messages.push('You have to chose a category for the product !!!');

    if ($scope.status_messages.length)
      return;

    var product = _.clone($scope.product_cpy);

    product.categories = [product.category];
    category_mgr.update_product(product, function(success, data)
    {
      if (success) // refresh product
        category_mgr.refresh_product();
      else
        $scope.status_messages.push(data);
    });

  };
})

// delete
.controller('del-product-ctrl', function($scope, category_mgr, category)
{
  $scope.status = {want_it: false, really: false};
  $scope.status_messages = [];

  $scope.del_product = function()
  {
    $scope.status_messages.length = 0;

    if (!$scope.status.want_it || !$scope.status.really)
      $scope.status_messages.push('You have to confirm the product removal process');

    if ($scope.status_messages.length)
      return;

    category_mgr.delete_product(category.active_product.id, function(success, data)
    {
      if (success) // go back to home
      {
        $scope.push_current_page('home', 'templates/main-page.html');
        $scope.clear_page_stack();
      }
      else
        $scope.status_messages.push(data);
    });

  };
});

