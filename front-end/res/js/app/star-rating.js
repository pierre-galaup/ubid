
// five star rating, based on a [0;100] score

angular.module('ubid')
.directive('starRating', function($tooltip)
{
  return {
    restrict: 'A',
    scope:
    {
      starRating: '&',
    },
    link: function(scope, elem, attrs)
    {
      elem.empty();

      elem.append('<span></span>');
      var span = elem.children();

      $tooltip(span, {title: function(a){return parseInt(scope.starRating(a)) + " %";}, placement: "right"});

      scope.$watch(scope.starRating, function(score)
      {
        span.empty();

        // append stars
        for (var i = 0; i < 5; ++i)
        {
          var completness = '';

          if ((i) * (100 / 5) + (1 * 100 / 5 / 2) > score)
          {
            completness = '-o';
          }
          else if ((i + 1) * (100 / 5) > score)
          {
            completness = '-half-o';
          }

          span.append('<i class="fa fa-star' + completness + ' rating-star"></i>');
        }
        if (score > 100)
          span.append('<i class="fa fa-plus rating-star-plus"></i>');
      });
    }
  };
});
