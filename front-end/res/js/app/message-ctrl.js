
angular.module('ubid')
.controller('message-ctrl', function($scope, user, user_mgr)
{
  $scope.show_message = function(el)
  {
    $(el).parents('.personnal-message').toggleClass('visible');
  }
  $scope.read_message = function(id, el)
  {
    $scope.show_message(el);
    user_mgr.mark_as_read(id);
  };
})
.controller('message-reply-ctrl', function($scope, user, user_mgr)
{
  $scope.reply =
  {
    content: "",
  };

  $scope.error_send_text = "";
  $scope.success_send_text = "";

  $scope.send_message = function()
  {
    if (!$scope.reply.content.length)
    {
      $scope.error_send_text = "You must enter a message !!!";
      $scope.success_send_text = "";

      return;
    }

    var dest = [$scope.message.author_id];
    _.each($scope.message.recipient_id, function(u)
    {
      if (u != user.id)
        dest.push(u);
    });
    user_mgr.send_message('RE: ' + $scope.message.subject, $scope.reply.content, dest, function(success, data)
    {
      if (success)
      {
        $scope.reply.content = "";

        $scope.error_send_text = "";
        $scope.success_send_text = "Message sent !!";
      }
      else
      {
        $scope.error_send_text = "Could not send the message";
        $scope.success_send_text = "";
      }

      user_mgr.poll_messages();
    });
  };
})

.controller('message-send-ctrl', function($scope, user, user_mgr)
{
  $scope.nmsg =
  {
    dest: "",
    title: "",
    content: "",
  };

  $scope.error_send_text = "";
  $scope.success_send_text = "";

  $scope.send_message = function()
  {
    $scope.success_send_text = "";
    $scope.error_send_text = "";
    if (!$scope.nmsg.content.length)
      $scope.error_send_text += "you must enter a message";
    if (!$scope.nmsg.title.length)
      $scope.error_send_text += ($scope.error_send_text.length ? ', ' : '') + "you must enter a title";
    if (!$scope.nmsg.dest.length)
      $scope.error_send_text += ($scope.error_send_text.length ? ', ' : '') + "you must enter at least somebody to send the message";

    if ($scope.error_send_text.length)
      return;

    user_mgr.send_message_to_users($scope.nmsg.title, $scope.nmsg.content, $scope.nmsg.dest.split(/ *; */), function(success, data)
    {
      if (success)
      {
        $scope.nmsg.content = "";
        $scope.nmsg.title = "";
        $scope.nmsg.dest = "";

        $scope.error_send_text = "";
        $scope.success_send_text = "Message sent !!";
      }
      else
      {
        $scope.error_send_text = "Could not send the message";
        $scope.success_send_text = "";
      }

      user_mgr.poll_messages();
    });
  };
})
