
// here, the deps are only thirs party
angular.module('ubid', ['ngAnimate', 'ngSanitize', 'mgcrea.ngStrap', 'pascalprecht.translate', 'http-auth-interceptor',
                        'chieffancypants.loadingBar', 'ubid-deps'])
.config(function($locationProvider, $translateProvider, $httpProvider)
{
  $locationProvider.html5Mode(true);
  $httpProvider.defaults.withCredentials = true;
//   $routeProvider.otherwise({templateUrl:'/404.html'});

  $translateProvider.useStaticFilesLoader(
  {
    prefix: 'locales/',
    suffix: '.json'
  });

})

// infos about the server (avoiding hardcoded strings)
.constant('server',
{
  url: 'http://localhost:5000',
  port: 5000,
  hostname: 'localhost',

  get_url: function(res)
  {
    return this.url + res;
  }
})

.directive('waitForLoad', function()
{
  return {
    restrict: 'A',
    link: function(scope, elem, attrs)
    {
      // once Angular is started, remove class:
      elem.removeClass('waiting-for-angular');
    }
  }
})

.controller('ubid-view', function($rootScope, user, $scope)
{
  $scope.user = user;
  return $scope;
})
.controller('user-auth-ctrl', function($scope, user_mgr)
{
  $scope.username = "";
  $scope.password = "";
  $scope.status = "";

  $scope.login = function()
  {
    if ($scope.username.length && $scope.password.length)
    {
      user_mgr.login($scope.username, $scope.password, function(success, message)
      {
        $scope.status = message;
      });
    }
    else
    {
      $scope.status = "please provide ";
      if (!$scope.username.length)
        $scope.status += "an username";
      else
        $scope.status += "a password";
      if (!$scope.username.length && !password.length)
        $scope.status += " and a password";
    }
  };
})
.controller('user-rate-ctrl', function($scope, user_mgr, category_mgr)
{
  $scope.status_message = "";
  $scope.new_score = null;
  $scope.success = false;

  $scope.rate_user = function()
  {
    $scope.status_message = "";
    if ($scope.new_score < 0 || $scope.new_score > 100)
    {
      $scope.status_message = "The score must be between 0 and 100.";
      return;
    }

    user_mgr.rate_user($scope.product.vendor.id, parseInt($scope.new_score), function(success, message)
    {
      if (success)
      {
        $scope.success = true;
        category_mgr.refresh_product();
      }
      else
      {
        if (message == "User already rate this user.")
        {
          $scope.success = true;
          $scope.status_message = "You have already rated this user !";
        }
        else
          $scope.status_message = message;
      }
    });
  };
})
.controller('user-register-ctrl', function($scope, user_mgr, page_stack, $timeout)
{
  $scope.username = "";
  $scope.password = "";
  $scope.mail = "";
  $scope.status = "";
  $scope.status_user = "";
  $scope.success = false;
  $scope.pop_page = _.bind(page_stack.pop_page, page_stack);

  $scope.register = function()
  {
    if ($scope.register_form.$valid && !$scope.success)
    {
      user_mgr.register($scope.username, $scope.password, $scope.mail,
      function(data, status) // success
      {
        $scope.success = true;
        $timeout(function()
        {
          page_stack.pop_page();
        }, 5000);
      },
      function(data, status) // error
      {
        if (status == 400 && data.message !== undefined)
          $scope.status_user = data.message;
        else if (data.message !== undefined)
          $scope.status = data.message;
      });
    }
  };
})
.controller('user-info-ctrl', function($scope, user_mgr, user, page_stack, $alert)
{
  $scope.user = angular.copy(user);
  $scope.password = "";
  $scope.creditcard = {number: null, cryptogram: null, expiration: null};
  $scope.pop_page = _.bind(page_stack.pop_page, page_stack);
  $scope.loading = false;
  $scope.status_user = "";

  $scope.save = function()
  {
    if ($scope.user_info_form.$valid)
    {
      $scope.status_user = "";
      $scope.loading = true;
      user_mgr.save($scope.user, $scope.password, $scope.changecreditcard ? $scope.creditcard : null, function(data, status) // success
      {
        angular.extend(user, $scope.user);
        $scope.loading = false;
        $alert({content: 'User info updated !', type: 'success', duration:2, placement:'top'});
      },
      function(data, status) // error
      {
        $scope.loading = false;
        if (status == 400 && data.message !== undefined)
          $scope.status_user = data.message;
        else if (data.message !== undefined)
          $alert({content: data.message, type: 'success', duration:2, placement:'top'});
        else
          $alert({content: 'Something went wrong...', type: 'danger', duration:2, placement:'top'});
      });
    }
  };
})
.controller('header-ctrl', function($scope, $http, server, user_mgr, user, page_stack, category, category_mgr)
{
  $scope.search_string = '';

  $scope.user = user;

  $scope.logout = _.bind(user_mgr.logout, user_mgr);
  $scope.push_current_page = _.partial(_.bind(page_stack.push_current_page, page_stack), $scope);
  $scope.pop_page = _.bind(page_stack.pop_page, page_stack);
  $scope.clear_page_stack = _.bind(page_stack.clear_page_stack, page_stack);

  $scope.search = function()
  {
    category_mgr.search_products($scope.search_string, false, function(success, message)
    {
      if (success)
      {
        category.active_category.name = "Search results for: '" + $scope.search_string + "'";
        $scope.push_current_page(category.active_category.name, 'templates/category-subcat-list.html', 'sub-cat-list-ctrl');
        $scope.clear_page_stack();
      }
    })
  }
})
.controller('menu-bar-ctrl', function($scope, user)
{
  $scope.user = user;
})

// cats
.controller('cat-view-ctrl', function($scope, products_data)
{
  $scope.category = products_data.active_category;
})

// something to boot from.
.controller('main-cat-list-ctrl', function($scope, category, category_mgr, root_category)
{
  $scope.category_name = "Categories";
  $scope.categories_list = [];
  category_mgr.get_category(root_category, function(success, message)
  {
    if (success)
      $scope.categories_list = category.active_category.categories;
  });

  $scope.view_category = function(id)
  {
    category_mgr.get_category($scope.categories_list[id].id, function(success, message)
    {
      if (success)
      {
        category.active_category.name = $scope.categories_list[id].name;
        $scope.push_current_page($scope.categories_list[id].name, 'templates/category-subcat-list.html', 'sub-cat-list-ctrl');
      }
    });
  };
})
// for every other category (except the main)
.controller('sub-cat-list-ctrl', function($scope, category, category_mgr)
{
  $scope.categories_list = category.active_category.categories;
  $scope.products_list = category.active_category.products;
  $scope.category_name = category.active_category.name;

  $scope.view_category = function(id)
  {
    category_mgr.get_category($scope.categories_list[id].id, function(success, message)
    {
      if (success)
      {
        category.active_category.name = $scope.categories_list[id].name;
        $scope.push_current_page($scope.categories_list[id].name, 'templates/category-subcat-list.html', 'sub-cat-list-ctrl');
      }
    });
  };
  $scope.view_product = function(id)
  {
    category_mgr.get_product($scope.products_list[id].id, function(success, message)
    {
      if (success)
        $scope.push_current_page($scope.products_list[id].name, 'templates/product-view.html', 'product-view-ctrl');
    });
  };
})

// products
.controller('product-view-ctrl', function($scope, category)
{
  $scope.product = category.active_product;
  $scope.categories = category.category_list;

  $scope.product_cpy = _.clone(category.active_product);
  $scope.product_cpy.category = $scope.product_cpy.categories[0];

})
.controller('product-bid-ctrl', function($scope, category_mgr)
{
  $scope.bid_status = '';
  $scope.bid_now = function()
  {
    if ($scope.bid_form.$valid)
    {
      category_mgr.bid_on_active_product($scope.bid_price, function(success, msg)
      {
        $scope.bid_status = msg;
      });
    }
  };
})
.controller('product-buy-ctrl', function($scope, category, user_mgr)
{
  $scope.bid_status = '';
  $scope.in_cart = user_mgr.is_in_cart(category.active_product);

  $scope.buy_now = function()
  {
  };

  $scope.add_to_cart = function()
  {
    user_mgr.add_to_cart(category.active_product);
    $scope.in_cart = true;
  };
})
.controller('cat-product-list-ctrl', function($scope, category, category_mgr)
{
  $scope.product_list = category.active_category.products;
  $scope.category_name = category.active_category.name;

  $scope.view_product = function(id)
  {
    category_mgr.get_product($scope.products_list[id].id, function(success, message)
    {
      if (success)
        $scope.push_current_page($scope.products_list[id].name, 'templates/product-view.html', 'product-view-ctrl');
    });
  };
})
.controller('stared-product-list-ctrl', function($scope, category, category_mgr, top_products)
{
  $scope.products_list = [];

  category_mgr.get_category_products(top_products, function(success, message)
  {
    if (success)
      $scope.products_list = category.active_category.products;
  });

  $scope.view_product = function(id)
  {
    category_mgr.get_product($scope.products_list[id].id, function(success, message)
    {
      if (success)
        $scope.push_current_page($scope.products_list[id].name, 'templates/product-view.html', 'product-view-ctrl');
    });
  };
})
.controller('fav-product-list-ctrl', function($scope, category, category_mgr, fav_products)
{
  $scope.category_name = "Favorites";
  $scope.products_list = [];

  category_mgr.get_category_products(fav_products, function(success, message)
  {
    if (success)
      $scope.products_list = category.active_category.products;
  });

  $scope.view_product = function(id)
  {
    category_mgr.get_product($scope.products_list[id].id, function(success, message)
    {
      if (success)
        $scope.push_current_page($scope.products_list[id].name, 'templates/product-view.html', 'product-view-ctrl');
    });
  };
})
.controller('cart-ctrl', function($scope, user, category_mgr, category)
{
  $scope.products_list = [];
  //FIXME Heavy -- A single API call to get multiple product could be usefull here
  _.each(user.cart, function(v, k) {
    category_mgr.get_product(user.cart[k], function(success, message)
    {
      if (success)
        $scope.products_list.push(category.active_product);
    });
  });
  
  $scope.view_product = function(id)
  {
    category.active_product = $scope.products_list[id];
    $scope.push_current_page($scope.products_list[id].name, 'templates/product-view.html', 'product-view-ctrl');
  };

  $scope.buy_cart = function()
  {
    //TODO And unsupported by api
  };
})