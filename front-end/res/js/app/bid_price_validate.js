angular.module('ubid')
.directive('validPrice', ['$http', function($http)
{
  return {
    require: 'ngModel',
    link: function(scope, ele, attrs, c)
    {
      scope.$watch(attrs.ngModel, function()
      {
        var price = $(ele).val();
        if (parseFloat(price) < scope.product.current_price + 0.01)
          c.$setValidity('price', false);
        else
          c.$setValidity('price', true);
      });
    }
  }
}]);